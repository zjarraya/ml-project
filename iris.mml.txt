%load est un
load 'iris.csv' as data
%

%%define predictive variable
% on
data.set_var_col("y"->last)

%%% define the graph structure %%%%
new_classifier model

model.add("clf"->"decisionTrees")

model.add("metric"->"accuracy")

model.add("test_size"->0.3)

%% evaluation in {cross_val, train_test_split}
model.add("evaluation"->"train_test_split")

%%%% strure defined %%%%

model.launch("data"->data)

show model.graph
show model.summary
