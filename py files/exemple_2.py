import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import tree
from sklearn.metrics import BIC_score

# Using pandas to import the dataset
df = pd.read_csv("base.csv")


# Spliting dataset between features (X) and label (y)
X = df.drop(columns=["valeur"])
y = df["valeur"]



# Spliting dataset into training set and test set
test_size = 0.2
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)



# Set algorithm to use
clf = tree.DecisionTreeClassifier()



# Use the algorithm to create a model with the training set
clf.fit(X_train, y_train)

# Compute and display the accuracy
BIC = BIC_score(y_test, clf.predict(X_test))

print(BIC)


